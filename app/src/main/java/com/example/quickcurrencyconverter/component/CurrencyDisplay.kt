package com.example.quickcurrencyconverter.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.indication
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import com.example.domain.model.Country
import com.example.quickcurrencyconverter.R

@Composable
fun CurrencyDisplay(
    country: Country,
    isBaseCurrency: Boolean,
    convertedAmount: String = "",
    onFlagClick: () -> Unit,
    onValueChanged: (String) -> Unit = {}
) {
    Column(
        modifier = Modifier
            .wrapContentHeight()
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        CountryFlag(flagUrl = country.flagUrl, onClick = onFlagClick)
        if (isBaseCurrency) {
            EditableCurrencyAmount(
                code = country.currencyCode,
                onValueChanged = onValueChanged
            )
        } else {
            CurrencyAmountText(amount = convertedAmount, code = country.currencyCode)
        }
    }
}

@Composable
private fun CountryFlag(flagUrl: String, onClick: () -> Unit) {
    val context = LocalContext.current
    val painter =
        rememberVectorPainter(image = ImageVector.vectorResource(id = R.drawable.error_icon))
    AsyncImage(
        modifier = Modifier
            .size(128.dp)
            .shadow(4.dp, shape = CircleShape)
            .clip(CircleShape)
            .indication(
                interactionSource = MutableInteractionSource(),
                indication = rememberRipple()
            )
            .clickable {
                onClick()
            },
        model = ImageRequest.Builder(context).data(flagUrl)
            .decoderFactory(SvgDecoder.Factory()).build(),
        fallback = painter,
        error = painter,
        contentScale = ContentScale.Crop,
        contentDescription = "Country Flag"
    )
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun EditableCurrencyAmount(code: String, onValueChanged: (String) -> Unit) {
    val keyboardController = LocalSoftwareKeyboardController.current
    var amount: String by remember { mutableStateOf("") }
    BasicTextField(
        value = amount,
        textStyle = LocalTextStyle.current.copy(
            textAlign = TextAlign.Center,
            fontSize = 30f.sp
        ),
        onValueChange = {
            amount = it
            onValueChanged(it)
        },
        keyboardOptions = KeyboardOptions(
            capitalization = KeyboardCapitalization.None,
            autoCorrect = false,
            keyboardType = KeyboardType.Decimal,
            imeAction = ImeAction.Done
        ),
        keyboardActions = KeyboardActions(
            onDone = {
                keyboardController?.hide()
            }
        ),
        singleLine = true,
        decorationBox = { innerTextField ->
            Row(horizontalArrangement = Arrangement.spacedBy(8.dp)) {
                innerTextField()
                CurrencyCodeText(code = code)
            }
        }
    )
}

@Composable
private fun CurrencyAmountText(amount: String, code: String) {
    Row(horizontalArrangement = Arrangement.spacedBy(8.dp)) {
        Text(text = amount, fontSize = 30f.sp)
        CurrencyCodeText(code = code)
    }
}

@Composable
private fun CurrencyCodeText(code: String) {
    Text(text = code, fontSize = 30f.sp)
}