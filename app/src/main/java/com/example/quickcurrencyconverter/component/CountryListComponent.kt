package com.example.quickcurrencyconverter.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import com.example.domain.model.Country

@Composable
fun CountryListComponent(
    modifier: Modifier = Modifier,
    countryList: List<Country>,
    onCountryClick: (country: Country) -> Unit
) {
    val state = rememberLazyListState()
    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        state = state,
        contentPadding = PaddingValues(vertical = 8.dp),
    ) {
        items(countryList, key = { item: Country -> item.name }) { country ->
            CountryColumnItem(country = country, onCountryClick = onCountryClick)
        }
    }
}

@Composable
fun CountryColumnItem(country: Country, onCountryClick: (country: Country) -> Unit) {
    val context = LocalContext.current
    Row(
        modifier = Modifier
            .height(60.dp)
            .fillMaxWidth()
            .clickable(
                interactionSource = MutableInteractionSource(),
                indication = rememberRipple()
            ) {
                onCountryClick.invoke(country)
            }
            .padding(horizontal = 8.dp),
        horizontalArrangement = Arrangement.Start,
        verticalAlignment = Alignment.CenterVertically
    ) {
        AsyncImage(
            modifier = Modifier
                .size(48.dp)
                .shadow(4.dp, shape = CircleShape)
                .clip(CircleShape),
            model = ImageRequest.Builder(context).data(country.flagUrl)
                .decoderFactory(SvgDecoder.Factory()).build(),
            contentDescription = null,
            contentScale = ContentScale.Crop
        )
        Spacer(modifier = Modifier.size(16.dp))
        Text(modifier = Modifier.weight(0.9f), text = country.name)
        Text(modifier = Modifier.weight(0.15f), text = country.currencyCode)
    }
}