package com.example.quickcurrencyconverter

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.core.view.WindowCompat
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.quickcurrencyconverter.screen.MainScreen
import com.example.quickcurrencyconverter.screen.NavGraphs
import com.example.quickcurrencyconverter.screen.PickerScreen
import com.example.quickcurrencyconverter.screen.destinations.MainScreenDestination
import com.example.quickcurrencyconverter.screen.destinations.PickerScreenDestination
import com.example.quickcurrencyconverter.ui.theme.QuickCurrencyConverterTheme
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.manualcomposablecalls.composable
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContent {
            val mainViewModel: MainViewModel = hiltViewModel()
            QuickCurrencyConverterTheme {
                DestinationsNavHost(
                    navGraph = NavGraphs.root
                ) {
                    composable(MainScreenDestination) {
                        MainScreen(viewModel = mainViewModel, navigator = destinationsNavigator)
                    }
                    composable(PickerScreenDestination) {
                        PickerScreen(
                            isBaseCurrency = this.navArgs.isBaseCurrency,
                            viewModel = mainViewModel,
                            navigator = destinationsNavigator
                        )
                    }
                }
            }
        }
    }
}