package com.example.quickcurrencyconverter

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.Empty
import com.example.domain.model.Country
import com.example.domain.usecase.GetCountryList
import com.example.domain.usecase.GetExchangeRates
import com.example.quickcurrencyconverter.util.CurrencySharedPrefHelper
import com.example.quickcurrencyconverter.util.to2dp
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@HiltViewModel
class MainViewModel @Inject constructor(
    private val getExchangeRates: GetExchangeRates,
    private val getCountryList: GetCountryList,
    private val sharedPrefHelper: CurrencySharedPrefHelper
) : ViewModel() {

    private val _countryList = MutableStateFlow(emptyList<Country>())
    val countryList = _countryList.asStateFlow()

    private val _selectedBaseCurrency =
        MutableStateFlow(sharedPrefHelper.getBaseCurrency() ?: Country())
    val selectedBaseCurrency = _selectedBaseCurrency.asStateFlow()

    private val _selectedConversionCurrency =
        MutableStateFlow(sharedPrefHelper.getConversionCurrency() ?: Country())
    val selectedConversionCurrency = _selectedConversionCurrency.asStateFlow()

    private val _currentRates = combine(
        _selectedBaseCurrency,
        _selectedConversionCurrency
    ) { base, conversion ->
        if (base.currencyCode.isEmpty() || conversion.currencyCode.isEmpty()) 1f
        else {
            withContext(Dispatchers.IO) {
                getExchangeRates.execute(base.currencyCode, conversion.currencyCode)?.rates
                    ?: 1f
            }
        }
    }.stateIn(viewModelScope, SharingStarted.Eagerly, 0f)

    private val _convertedValue: MutableStateFlow<String> = MutableStateFlow("")
    val convertedValue = _convertedValue.asStateFlow()

    init {
        getCountryCodeList()
    }

    private fun getCountryCodeList() {
        viewModelScope.launch(Dispatchers.IO) {
            _countryList.emit(getCountryList.execute())
        }
    }

    fun selectBaseCurrency(country: Country) = viewModelScope.launch {
        _selectedBaseCurrency.emit(country)
        sharedPrefHelper.updateBaseCurrency(country)
    }

    fun selectConversionCurrency(country: Country) = viewModelScope.launch {
        _selectedConversionCurrency.emit(country)
        sharedPrefHelper.updateConversionCurrency(country)
    }

    fun convert(value: String) = viewModelScope.launch {
        val valueInFloat = value.toFloatOrNull()
        valueInFloat?.let {
            _convertedValue.emit(
                (it * _currentRates.value).to2dp().toString()
            )
        } ?: _convertedValue.emit(String.Empty)
    }

    fun reset() = viewModelScope.launch {
        _convertedValue.emit(String.Empty)
    }
}