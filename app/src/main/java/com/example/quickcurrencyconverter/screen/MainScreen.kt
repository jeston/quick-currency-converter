package com.example.quickcurrencyconverter.screen

import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import com.example.quickcurrencyconverter.MainViewModel
import com.example.quickcurrencyconverter.R
import com.example.quickcurrencyconverter.component.CurrencyDisplay
import com.example.quickcurrencyconverter.screen.destinations.PickerScreenDestination
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@OptIn(ExperimentalMaterial3Api::class)
@RootNavGraph(start = true)
@Destination
@Composable
fun MainScreen(
    viewModel: MainViewModel,
    navigator: DestinationsNavigator,
) {
    val baseCurrency = viewModel.selectedBaseCurrency.collectAsState()
    val conversionCurrency = viewModel.selectedConversionCurrency.collectAsState()
    val convertedValue = viewModel.convertedValue.collectAsState()

    LaunchedEffect(baseCurrency.value, conversionCurrency.value) {
        viewModel.reset()
    }

    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(text = "Currency Converter")
                },
                navigationIcon = {
                },
                actions = {
                    IconButton(
                        onClick = {
                            Log.i("LELEL", "Icon button clicked")
                        },
                    ) {
                        Icon(
                            imageVector = ImageVector.vectorResource(id = R.drawable.more_vert),
                            contentDescription = "More Icon"
                        )
                    }
                }
            )
        },
        bottomBar = {
        },
        snackbarHost = {

        },
        floatingActionButton = {

        },
        content = { padding ->
            Column(
                modifier = Modifier
                    .padding(padding)
                    .fillMaxSize()
                    .imePadding(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.SpaceEvenly
            ) {
                CurrencyDisplay(
                    country = baseCurrency.value,
                    isBaseCurrency = true,
                    onFlagClick = {
                        navigator.navigate(PickerScreenDestination(isBaseCurrency = true))
                    },
                    onValueChanged = { value ->
                        viewModel.convert(value)
                    }
                )
                CurrencyDisplay(
                    country = conversionCurrency.value,
                    isBaseCurrency = false,
                    convertedAmount = convertedValue.value.toString(),
                    onFlagClick = {
                        navigator.navigate(PickerScreenDestination(isBaseCurrency = false))
                    })
            }
        }
    )
}