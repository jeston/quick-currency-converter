package com.example.quickcurrencyconverter.screen

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import com.example.quickcurrencyconverter.MainViewModel
import com.example.quickcurrencyconverter.R
import com.example.quickcurrencyconverter.component.CountryListComponent
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@OptIn(ExperimentalMaterial3Api::class)
@Destination
@Composable
fun PickerScreen(
    isBaseCurrency: Boolean,
    viewModel: MainViewModel,
    navigator: DestinationsNavigator
) {
    val countryList = viewModel.countryList.collectAsState()
    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(
                modifier = Modifier.shadow(4.dp),
                title = {
                    Text(text = "Currency List")
                },
                navigationIcon = {
                    IconButton(
                        onClick = {
                            navigator.popBackStack()
                        }) {
                        Icon(
                            modifier = Modifier.size(24.dp),
                            imageVector = ImageVector.vectorResource(id = R.drawable.ic_black_arrow_left),
                            contentDescription = "Back button"
                        )
                    }
                },
                actions = {
                }
            )
        },
        bottomBar = {
        },
        snackbarHost = {
        },
        floatingActionButton = {
        },
        content = { padding ->
            CountryListComponent(
                modifier = Modifier.padding(padding),
                countryList = countryList.value,
                onCountryClick = {
                    if (isBaseCurrency) {
                        viewModel.selectBaseCurrency(it).invokeOnCompletion { throwable ->
                            if (throwable == null) {
                                navigator.popBackStack()
                            } else {
                                throw throwable
                            }
                        }
                    } else {
                        viewModel.selectConversionCurrency(it).invokeOnCompletion { throwable ->
                            if (throwable == null) {
                                navigator.popBackStack()
                            } else {
                                throw throwable
                            }
                        }
                    }
                }
            )
        }
    )
}