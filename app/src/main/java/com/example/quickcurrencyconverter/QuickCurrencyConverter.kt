package com.example.quickcurrencyconverter

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class QuickCurrencyConverter : Application() {
}