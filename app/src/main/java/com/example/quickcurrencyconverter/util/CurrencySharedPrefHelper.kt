package com.example.quickcurrencyconverter.util

import android.content.Context
import android.content.SharedPreferences
import com.example.domain.model.Country
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class CurrencySharedPrefHelper(
    context: Context
) {
    private val baseSp: SharedPreferences =
        context.getSharedPreferences(BASE_CURRENCY_SP, Context.MODE_PRIVATE)
    private val conversionSp: SharedPreferences =
        context.getSharedPreferences(CONVERSION_CURRENCY_SP, Context.MODE_PRIVATE)

    fun updateBaseCurrency(country: Country) {
        val json = Json.encodeToString(country)
        with(baseSp.edit()) {
            putString(BASE_COUNTRY, json)
            commit()
        }
    }

    fun updateConversionCurrency(country: Country) {
        val json = Json.encodeToString(country)
        with(conversionSp.edit()) {
            putString(CONVERSION_COUNTRY, json)
            commit()
        }
    }

    fun getBaseCurrency(): Country? {
        with(baseSp) {
            val json = this.getString(BASE_COUNTRY, null)
            json?.let {
                return Json.decodeFromString(it)
            } ?: return null
        }
    }

    fun getConversionCurrency(): Country? {
        with(conversionSp) {
            val json = this.getString(CONVERSION_COUNTRY, null)
            json?.let {
                return Json.decodeFromString(it)
            } ?: return null
        }
    }

    companion object {
        const val BASE_CURRENCY_SP = "selected_base_currency_sp"
        const val CONVERSION_CURRENCY_SP = "selected_conversion_currency_sp"
        const val BASE_COUNTRY = "base_country"
        const val CONVERSION_COUNTRY = "conversion_country"
    }
}