package com.example.quickcurrencyconverter.util

import java.math.RoundingMode
import java.text.DecimalFormat

fun Float.to2dp(): Float {
    with(DecimalFormat("#.##").apply {
        roundingMode = RoundingMode.CEILING
    }) {
        return this.format(this@to2dp).toFloat()
    }
}