package com.example.quickcurrencyconverter.di

import com.example.domain.usecase.APIGetCurrencySymbols
import com.example.domain.usecase.APIGetExchangeRates
import com.example.domain.usecase.GetCountryList
import com.example.domain.usecase.APIGetCountryList
import com.example.domain.usecase.GetCurrencySymbols
import com.example.domain.usecase.GetExchangeRates
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class ApiModules {
    @Binds
    abstract fun bindGetCurrencySymbols(getCurrencySymbols: APIGetCurrencySymbols): GetCurrencySymbols

    @Binds
    abstract fun bindGetExchangeRates(getExchangeRates: APIGetExchangeRates): GetExchangeRates

    @Binds
    abstract fun bindGetCountryList(getCountryListImpl: APIGetCountryList): GetCountryList
}