package com.example.quickcurrencyconverter.di

import android.content.Context
import com.example.quickcurrencyconverter.util.CurrencySharedPrefHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UtilityModule {
    @Provides
    @Singleton
    fun provideCurrencySharedPrefHelper(@ApplicationContext context: Context): CurrencySharedPrefHelper {
        return CurrencySharedPrefHelper(context)
    }
}