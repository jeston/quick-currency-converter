package com.example.quickcurrencyconverter

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.domain.model.Country
import com.example.quickcurrencyconverter.util.CurrencySharedPrefHelper
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CurrencySharedPrefHelperTest {

    private lateinit var helper: CurrencySharedPrefHelper
    private lateinit var dummyCountry: Country

    @Before
    fun setup() {
        helper = CurrencySharedPrefHelper(InstrumentationRegistry.getInstrumentation().context)
        dummyCountry = Country(
            name = "Singapore",
            currencyCode = "SGD",
            flagUrl = "random_url"
        )
    }

    @Test
    fun updateBaseCurrencyToSharedPref() {
        helper.updateBaseCurrency(dummyCountry)
        val result = helper.getBaseCurrency()
        MatcherAssert.assertThat(dummyCountry, CoreMatchers.equalTo(result))
    }

    @Test
    fun updateConversionCurrencyToSharedPref() {
        helper.updateConversionCurrency(dummyCountry)
        val result = helper.getConversionCurrency()
        MatcherAssert.assertThat(dummyCountry, CoreMatchers.equalTo(result))
    }
}