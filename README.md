This demo app seek to follow closely with the MVVM design pattern

# Modules

### `data` layer
- Contains the repository which handles data retrieval from either an API or from a local database
- API calls are handled by Retrofit
- Local database is setup with Room

### `domain` layer
- Contains the Usecases which encapsulates the business logic and can be reused by multiple ViewModels
- Contains the model which is used directly by the UI to display information

### `app` (UI) layer
- Contains the typical UI components and navigation logics
- Built with Compose