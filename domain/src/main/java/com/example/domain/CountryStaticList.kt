package com.example.domain

object CountryStaticList {
    private val currencyCountryMapping = mapOf(
        "USD" to "US",
        "EUR" to "EU",
        "JPY" to "JP",
        "GBP" to "GB",
        "AUD" to "AU",
        "CAD" to "CA",
        "CHF" to "CH",
        "CNY" to "CN",
        "NZD" to "NZ",
        "HKD" to "HK",
        "SEK" to "SE",
        "KRW" to "KR",
        "SGD" to "SG",
        "NOK" to "NO",
        "MXN" to "MX",
        "INR" to "IN",
        "RUB" to "RU",
        "ZAR" to "ZA",
        "TRY" to "TR",
        "BRL" to "BR",
        "TWD" to "TW",
        "PLN" to "PL",
        "DKK" to "DK",
        "MYR" to "MY",
        "THB" to "TH",
        "IDR" to "ID",
        "PHP" to "PH",
        "HUF" to "HU",
        "CZK" to "CZ",
        "ILS" to "IL"
    )

    fun getCurrencyList(order: SortOrder = SortOrder.Popular): List<String> {
        return when (order) {
            is SortOrder.Popular -> {
                currencyCountryMapping.keys.toList()
            }
            is SortOrder.Alphabetical -> {
                currencyCountryMapping.keys.toList().sorted()
            }
        }
    }

    fun getCountryCodeList(order: SortOrder = SortOrder.Popular): List<String> {
        return when (order) {
            is SortOrder.Popular -> {
                currencyCountryMapping.values.toList()
            }
            is SortOrder.Alphabetical -> {
                currencyCountryMapping.values.toList().sorted()
            }
        }
    }

    fun getCountryCode(currencyCode: String): String {
        return currencyCountryMapping[currencyCode]!!
    }

    fun getCurrencyCode(countryCode: String): String {
        return currencyCountryMapping.filterValues { it == countryCode }.keys.first()
    }

    sealed interface SortOrder {
        object Popular : SortOrder
        object Alphabetical : SortOrder
    }
}