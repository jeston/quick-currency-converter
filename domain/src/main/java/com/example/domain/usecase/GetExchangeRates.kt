package com.example.domain.usecase

import com.example.data.repository.ExchangeRatesRepository
import com.example.domain.model.ExchangeRatesBundle
import javax.inject.Inject

interface GetExchangeRates {
    /**
     * [conversion]: a list of strings separated by ','
     */
    suspend fun execute(base: String, conversion: String): ExchangeRatesBundle?
}

class APIGetExchangeRates @Inject constructor(
    private val repository: ExchangeRatesRepository
) : GetExchangeRates {
    override suspend fun execute(base: String, conversion: String): ExchangeRatesBundle? {
        val response = repository.getExchangeRates(base, conversion) ?: return null
        return ExchangeRatesBundle(
            base = response.base,
            conversion = response.rates.keys.first(),
            date = response.date,
            rates = response.rates.values.first().toFloat(),
            timestamp = response.timestamp
        )
    }
}