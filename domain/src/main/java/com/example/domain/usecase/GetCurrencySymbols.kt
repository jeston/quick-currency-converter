package com.example.domain.usecase

import com.example.data.repository.ExchangeRatesRepository
import com.example.domain.model.CurrencySymbolsBundle
import javax.inject.Inject

interface GetCurrencySymbols {
    suspend fun execute(): CurrencySymbolsBundle?
}

class APIGetCurrencySymbols @Inject constructor(
    private val repository: ExchangeRatesRepository
) : GetCurrencySymbols {
    override suspend fun execute(): CurrencySymbolsBundle? {
        val response = repository.getCurrencySymbols() ?: return null
        return CurrencySymbolsBundle(
            symbols = response.symbols
        )
    }
}