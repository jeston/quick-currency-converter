package com.example.domain.usecase

import com.example.data.repository.CountryRepository
import com.example.domain.CountryStaticList
import com.example.domain.model.Country
import javax.inject.Inject

interface GetCountryList {
    suspend fun execute(): List<Country>
}

class APIGetCountryList @Inject constructor(
    private val repository: CountryRepository
) : GetCountryList {
    override suspend fun execute(): List<Country> {
        val countryMap = repository.getCountryList().countryMap
        return CountryStaticList.getCountryCodeList(CountryStaticList.SortOrder.Alphabetical)
            .map { code ->
                Country(
                    name = countryMap[code.lowercase()]!!,
                    currencyCode = CountryStaticList.getCurrencyCode(code),
                    flagUrl = "https://flagcdn.com/${code.lowercase()}.svg"
                )
            }
    }
}