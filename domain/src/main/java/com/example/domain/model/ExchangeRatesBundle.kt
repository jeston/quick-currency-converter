package com.example.domain.model

data class ExchangeRatesBundle(
    val base: String,
    val conversion: String,
    val date: String,
    val rates: Float,
    val timestamp: Long
)
