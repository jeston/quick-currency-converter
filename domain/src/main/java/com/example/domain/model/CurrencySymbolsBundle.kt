package com.example.domain.model

data class CurrencySymbolsBundle(
    val symbols: Map<String, String>
)
