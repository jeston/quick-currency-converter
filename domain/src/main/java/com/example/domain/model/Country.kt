package com.example.domain.model

import com.example.domain.Empty
import kotlinx.serialization.Serializable

@Serializable
data class Country(
    val name: String = String.Empty,
    val currencyCode: String = String.Empty,
    val flagUrl: String = String.Empty
) {
    fun isEmptyCountry(): Boolean {
        return name.isEmpty() && currencyCode.isEmpty() && flagUrl.isEmpty()
    }
}