package com.example.data

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.data.local.ConversionPair
import com.example.data.local.ConversionPairDao
import com.example.data.local.ExchangeRatesDatabase
import java.io.IOException
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ConversionPairDaoTest {

    private lateinit var dao: ConversionPairDao
    private lateinit var db: ExchangeRatesDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, ExchangeRatesDatabase::class.java
        ).build()
        dao = db.conversionPairDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeConversionPairAndRead() {
        val pairing = ConversionPair(
            uid = "SGD:USD",
            baseCurrency = "SGD",
            conversionCurrency = "USD",
            rate = 1.3f,
            timestamp = 1L,
            date = "2021-03-17"
        )
        dao.insertPairing(pairing)
        val result = dao.getPairing("SGD", "USD")
        assertThat(pairing, equalTo(result))
    }

    @Test
    fun writeConversionPairAndDelete() {
        val pairing = ConversionPair(
            uid = "SGD:USD",
            baseCurrency = "SGD",
            conversionCurrency = "USD",
            rate = 1.3f,
            timestamp = 1L,
            date = "2021-03-17"
        )
        dao.insertPairing(pairing)
        dao.deletePairing(pairing)
        val deletedResult = dao.getPairing("SGD", "USD")
        assertThat(null, equalTo(deletedResult))
    }

    @Test
    fun updateExistingConversionPair() {
        val initialPairing = ConversionPair(
            uid = "SGD:USD",
            baseCurrency = "SGD",
            conversionCurrency = "USD",
            rate = 1.3f,
            timestamp = 1L,
            date = "2021-03-17"
        )
        dao.insertPairing(initialPairing)
        val updatedPairing = ConversionPair(
            uid = "SGD:USD",
            baseCurrency = "SGD",
            conversionCurrency = "USD",
            rate = 1.4f,
            timestamp = 1L,
            date = "2021-03-17"
        )
        dao.updatePairing(updatedPairing)
        val updatedResult = dao.getPairing("SGD", "USD")
        assertThat(updatedPairing, equalTo(updatedResult))
    }

    @Test
    fun updateNonExistingConversionPair() {
        val initialPairing = ConversionPair(
            uid = "SGD:USD",
            baseCurrency = "SGD",
            conversionCurrency = "USD",
            rate = 1.3f,
            timestamp = 1L,
            date = "2021-03-17"
        )
        dao.insertPairing(initialPairing)
        val updatedPairing = ConversionPair(
            uid = "SGD:MYR",
            baseCurrency = "SGD",
            conversionCurrency = "MYR",
            rate = 3f,
            timestamp = 1L,
            date = "2021-03-17"
        )
        dao.updatePairing(updatedPairing)
        val updatedResult = dao.getPairing("SGD", "MYR")
        assertThat(null, equalTo(updatedResult))
    }

}