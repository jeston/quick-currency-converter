package com.example.data

import com.example.data.repository.DateComparisonHelper
import io.mockk.every
import io.mockk.mockkStatic
import java.time.LocalDate
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class DateComparisonHelperTest {

    @Before
    fun setupTestDate() {
        val testDate = LocalDate.of(2022, 4, 21)

        mockkStatic(LocalDate::class)
        every { LocalDate.now() } returns testDate
    }

    @Test
    fun testRecentDate() {
        assertThat(false, equalTo(DateComparisonHelper.isOutdated("2022-04-20")))
    }

    @Test
    fun testOutdatedDate() {
        assertThat(true, equalTo(DateComparisonHelper.isOutdated("2022-04-19")))
    }
}