package com.example.data.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [ConversionPair::class], version = 1)
abstract class ExchangeRatesDatabase : RoomDatabase() {
    abstract fun conversionPairDao(): ConversionPairDao
}