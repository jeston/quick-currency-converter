package com.example.data.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface ConversionPairDao {
    @Query("SELECT * FROM conversionpair WHERE base = :base AND conversion = :conversion")
    fun getPairing(base: String, conversion: String): ConversionPair?

    @Insert
    fun insertPairing(coversionPair: ConversionPair)

    @Update
    fun updatePairing(conversionPair: ConversionPair)

    @Delete
    fun deletePairing(conversionPair: ConversionPair)
}