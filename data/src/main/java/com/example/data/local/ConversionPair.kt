package com.example.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ConversionPair(
    @PrimaryKey val uid: String,
    @ColumnInfo(name = "base") val baseCurrency: String,
    @ColumnInfo(name = "conversion") val conversionCurrency: String,
    @ColumnInfo(name = "rate") val rate: Float,
    @ColumnInfo(name = "date") val date: String,
    @ColumnInfo(name = "timestamp") val timestamp: Long
)
