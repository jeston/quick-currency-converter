package com.example.data.remote

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET

interface CountryApi {
    @GET("codes.json")
    fun getCountryList(): Call<JsonObject>
}

data class CountryListResponse(
    val countryMap: Map<String, String>
)

