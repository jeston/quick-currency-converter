package com.example.data.remote

import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ExchangeRatesApi {
    @GET("symbols")
    fun getCountrySymbols(): Call<CurrencySymbolsResponse>

    @GET("latest")
    fun getLatestRates(
        @Query("base") base: String,
        @Query("symbols") symbols: String
    ): Call<CurrencyRatesResponse>
}

data class CurrencySymbolsResponse(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("symbols")
    val symbols: Map<String, String>
)

data class CurrencyRatesResponse(
    @SerializedName("base")
    val base: String,
    @SerializedName("date")
    val date: String,
    @SerializedName("rates")
    val rates: Map<String, Double>,
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("timestamp")
    val timestamp: Long
)