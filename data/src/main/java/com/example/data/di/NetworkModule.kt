package com.example.data.di

import android.content.Context
import androidx.room.Room
import com.example.data.BuildConfig
import com.example.data.local.ConversionPairDao
import com.example.data.local.ExchangeRatesDatabase
import com.example.data.remote.CountryApi
import com.example.data.remote.ExchangeRatesApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Provides
    @Singleton
    fun providesRetrofitForExchangeRateApi(): ExchangeRatesApi {
        val client = OkHttpClient.Builder().addInterceptor {
            val request = it.request()
            val updatedHeaders =
                request.headers().newBuilder().add("apikey", BuildConfig.EXCHANGE_RATE_DATA_API)
                    .build()
            val updatedRequest = request.newBuilder().headers(updatedHeaders).build()
            it.proceed(updatedRequest)
        }.build()
        return Retrofit.Builder()
            .client(client)
            .baseUrl(BuildConfig.EXCHANGE_RATE_DATA_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ExchangeRatesApi::class.java)
    }

    @Provides
    @Singleton
    fun providesRetrofitForCountryApi(): CountryApi {
        return Retrofit.Builder()
            .baseUrl("https://flagcdn.com/en/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(CountryApi::class.java)
    }

    @Provides
    @Singleton
    fun providesConversionPairDao(
        @ApplicationContext context: Context
    ): ConversionPairDao {
        val database = Room.databaseBuilder(
            context,
            ExchangeRatesDatabase::class.java,
            "exchange-rates-database"
        ).build()
        return database.conversionPairDao()
    }
}