package com.example.data.repository

import com.example.data.local.ConversionPair
import com.example.data.local.ConversionPairDao
import com.example.data.remote.CurrencyRatesResponse
import com.example.data.remote.CurrencySymbolsResponse
import com.example.data.remote.ExchangeRatesApi
import javax.inject.Inject
import retrofit2.awaitResponse

class ExchangeRatesRepository @Inject constructor(
    private val exchangeRatesApi: ExchangeRatesApi,
    private val exchangeRatesDao: ConversionPairDao
) {
    suspend fun getCurrencySymbols(): CurrencySymbolsResponse? {
        val response = exchangeRatesApi.getCountrySymbols().awaitResponse()
        if (response.isSuccessful) {
            return response.body()
        } else {
            throw UnknownError()
        }
    }

    // TODO simplify this
    suspend fun getExchangeRates(base: String, conversion: String): CurrencyRatesResponse? {
        // check if Room has an entry that's at most 1 day old, if not call API
        val existingPairing = exchangeRatesDao.getPairing(base, conversion)
        if (existingPairing == null) {
            val response = exchangeRatesApi.getLatestRates(base, conversion).awaitResponse()
            if (response.isSuccessful) {
                val body = response.body()
                body?.let {
                    exchangeRatesDao.insertPairing(
                        ConversionPair(
                            uid = "${body.base}:${body.rates.keys.first()}",
                            baseCurrency = body.base,
                            conversionCurrency = body.rates.keys.first(),
                            rate = body.rates.entries.first().value.toFloat(),
                            date = body.date,
                            timestamp = body.timestamp
                        )
                    )
                }
                return body
            } else {
                throw UnknownError()
            }
        } else {
            val date = existingPairing.date
            val isOutdated = DateComparisonHelper.isOutdated(date)
            if (isOutdated) {
                val response = exchangeRatesApi.getLatestRates(base, conversion).awaitResponse()
                if (response.isSuccessful) {
                    val body = response.body()
                    body?.let {
                        exchangeRatesDao.updatePairing(
                            ConversionPair(
                                uid = "${body.base}:${body.rates.keys.first()}",
                                baseCurrency = body.base,
                                conversionCurrency = body.rates.keys.first(),
                                rate = body.rates.entries.first().value.toFloat(),
                                date = body.date,
                                timestamp = body.timestamp
                            )
                        )
                    }
                    return body
                } else {
                    throw UnknownError()
                }
            } else {
                return CurrencyRatesResponse(
                    base = existingPairing.baseCurrency,
                    date = existingPairing.date,
                    rates = mapOf(existingPairing.conversionCurrency to existingPairing.rate.toDouble()),
                    success = true,
                    timestamp = existingPairing.timestamp
                )
            }
        }
    }
}

