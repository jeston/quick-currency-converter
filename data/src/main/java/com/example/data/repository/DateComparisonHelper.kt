package com.example.data.repository

import java.time.LocalDate
import java.time.format.DateTimeFormatter

object DateComparisonHelper {

    private val pattern = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    private val todayDate get() = LocalDate.now()

    fun isOutdated(givenDate: String): Boolean {
        val date = LocalDate.parse(givenDate, pattern)
        return todayDate.minusDays(date.toEpochDay()).toEpochDay() >= 2L
    }
}