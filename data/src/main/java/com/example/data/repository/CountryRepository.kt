package com.example.data.repository

import com.example.data.remote.CountryApi
import com.example.data.remote.CountryListResponse
import javax.inject.Inject
import retrofit2.awaitResponse

class CountryRepository @Inject constructor(
    private val countryApi: CountryApi
) {
    suspend fun getCountryList(): CountryListResponse {
        val response = countryApi.getCountryList().awaitResponse()
        if (response.isSuccessful) {
            val map = mutableMapOf<String, String>()
            val set = response.body()?.entrySet()
                ?: throw UnknownError("Empty response returned for list of countries")
            set.forEach {
                map[it.key] = it.value.asString
            }
            return CountryListResponse(map)
        } else {
            throw UnknownError("Failed to get list of countries")
        }
    }
}